Deutschland. Ein Wintermärchen
Heinrich Heine

Caput I

Im traurigen Monat November war's,
Die Tage wurden trüber,
Der Wind riß von den Bäumen das Laub,
Da reist ich nach Deutschland hinüber.

Und als ich an die Grenze kam,
Da fühlt ich ein stärkeres Klopfen
In meiner Brust, ich glaube sogar
Die Augen begunnen zu tropfen.

Und als ich die deutsche Sprache vernahm,
Da ward mir seltsam zumute;
Ich meinte nicht anders, als ob das Herz
Recht angenehm verblute.
