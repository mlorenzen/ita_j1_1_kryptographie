package blatt3;

import java.io.BufferedReader;
import java.io.StringReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Random;

public class AufgabeP1 {

	public static void main(String[] args) throws Exception {
		/*
		 * Programm zur Verschlüsselung einer Textdatei mit dem
		 * One-Time-Pad-Verfahren
		 * 
		 * Zur Vereinfachung wird zeilenweise verschlüsselt,
		 * und es werden auch für nicht verschlüsselte Zeichen
		 * Schlüssel geschrieben
		 */
		String buchstaben = "abcdefghijklmnopqrstuvwxyzäöüß";
		String klartextDatei = "klartext.txt";
		String chiffretextDatei = "chiffretext.txt";
		String schluesselDatei = "schluessel.txt";

		StringBuilder chiffreSammler = new StringBuilder();
		StringBuilder schluesselSammler = new StringBuilder();

		BufferedReader textZeile = new BufferedReader(new StringReader(getFileContentLc(klartextDatei)));
		String zeile = null;

		while ((zeile = textZeile.readLine()) != null) {
			for (int i = 0; i < zeile.length(); i++) {
				char c = erzeugeZufallsChar(buchstaben);
				if (buchstaben.indexOf(zeile.charAt(i)) >= 0) {
					chiffreSammler.append(zeichenVerschluesseln(zeile.charAt(i), c, buchstaben));
				} else {
					chiffreSammler.append(zeile.charAt(i));
				}
				schluesselSammler.append(c);
			}
			chiffreSammler.append(System.lineSeparator());
			schluesselSammler.append(System.lineSeparator());
		}


		// Ausgaben speichern
		saveTextFile(chiffreSammler.toString(), chiffretextDatei);
		saveTextFile(schluesselSammler.toString(), schluesselDatei);

	}

	private static char zeichenVerschluesseln(char klarC, char schluesselC, String alphabet) {
		int positionInAlphabet = alphabet.indexOf(klarC);
		int verschiebung = alphabet.indexOf(schluesselC);
		int neuePosition = (positionInAlphabet + verschiebung) % alphabet.length();
		return alphabet.charAt(neuePosition);
	}

	private static String getFileContentLc(String dateiName) throws Exception {
		/// liest eine Textdatei und gibt den Inhalt in kleinschreibung zurück
		File textDatei = new File(dateiName);
		FileReader fr = new FileReader(textDatei);
		BufferedReader reader = new BufferedReader(fr);
		String line = null;
		StringBuilder textCollector = new StringBuilder();
		while (true) {
			line = reader.readLine();
			if (line == null)
				break;
			textCollector.append(line.toLowerCase());
			textCollector.append(System.lineSeparator());
		}
		reader.close();
		fr.close();
		return textCollector.toString();
	}

	private static void saveTextFile(String dateiText, String dateiName) throws Exception {
		/// Schreibt eine Textdatei in das Filesystem
		File ausgabeDatei = new File(dateiName);
		FileWriter fw = new FileWriter(ausgabeDatei);
		fw.write(dateiText);
		fw.close();
	}

	private static char erzeugeZufallsChar(String alphabet) {
		/// Gibt einen zufälligen Buchstaben aus dem Alphabet zurück
		Random zufall = new Random();
		return alphabet.charAt(zufall.nextInt(alphabet.length()));
	}

}
