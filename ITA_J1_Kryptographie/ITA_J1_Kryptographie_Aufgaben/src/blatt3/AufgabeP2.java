package blatt3;

import java.io.BufferedReader;
import java.io.StringReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class AufgabeP2 {

	public static void main(String[] args) throws Exception {
		/*
		 * Das Programm entschlüsselt die mit AufgabeP1 erstellten
		 * Chiffretexte, gibt sie auf der Konsole aus
		 *  und speichert sie in eine Datei
		 *  
		 *  Auf jegliche Konsistenzprüfung der Eingabedatei wurde verzichtet,
		 *  es wird davon ausgegangen dass die Eingabedateien zueinander
		 *  passen.
		 */
		String buchstaben = "abcdefghijklmnopqrstuvwxyzäöüß";
		String klartextDatei = "decrypted.txt";
		String chiffretextDatei = "chiffretext.txt";
		String schluesselDatei = "schluessel.txt";
		String chiffreText = getFileContentLc(chiffretextDatei);
		String schluesselText = getFileContentLc(schluesselDatei);
		
		StringBuilder decryptSammler = new StringBuilder();
		BufferedReader chiffreReader = new BufferedReader(new StringReader(chiffreText));
		BufferedReader schluesselReader = new BufferedReader(new StringReader(schluesselText));
		
		String chiffreZeile = null;
		String schluesselZeile = null;
		
		while ((chiffreZeile = chiffreReader.readLine()) != null && 
				(schluesselZeile = schluesselReader.readLine()) != null ) {
			for (int i = 0; i < chiffreZeile.length(); i++){
				if (buchstaben.indexOf(chiffreZeile.charAt(i)) >= 0){
					decryptSammler.append(zeichenEntschluesseln(chiffreZeile.charAt(i), 
							schluesselZeile.charAt(i), buchstaben));
				} else {
					decryptSammler.append(chiffreZeile.charAt(i));
				}
			}
			decryptSammler.append(System.lineSeparator());
		}
		saveTextFile(decryptSammler.toString(), klartextDatei);
		
		System.out.println("Der Klartext lautet: \n");
		System.out.println(decryptSammler.toString());
		
	}
	
	private static char zeichenEntschluesseln(char chiffreC, char schluesselC, String alphabet) {
		int positionInAlphabet = alphabet.indexOf(chiffreC);
		int verschiebung = alphabet.indexOf(schluesselC);
		int neuePosition = (positionInAlphabet - verschiebung) % alphabet.length();
		if (neuePosition < 0) neuePosition = neuePosition + alphabet.length();
		return alphabet.charAt(neuePosition);
	}
	
	private static void saveTextFile(String dateiText, String dateiName) throws Exception {
		/// Schreibt eine Textdatei in das Filesystem
		File ausgabeDatei = new File(dateiName);
		FileWriter fw = new FileWriter(ausgabeDatei);
		fw.write(dateiText);
		fw.close();
	}
	
	private static String getFileContentLc(String dateiName) throws Exception {
		/// liest eine Textdatei und gibt den Inhalt in kleinschreibung zurück
		File textDatei = new File(dateiName);
		FileReader fr = new FileReader(textDatei);
		BufferedReader reader = new BufferedReader(fr);
		String line = null;
		StringBuilder textCollector = new StringBuilder();
		while (true) {
			line = reader.readLine();
			if (line == null)
				break;
			textCollector.append(line.toLowerCase());
			textCollector.append(System.lineSeparator());
		}
		reader.close();
		fr.close();
		return textCollector.toString();
	}

}
