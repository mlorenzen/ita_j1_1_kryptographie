package mlorenzen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class AufgabeP2 {

	//KK: gut :) nur Kleinigkeiten, siehe Kommentare
	public static void main(String[] args) throws Exception {

		String buchstaben = "abcdefghijklmnopqrstuvwxyzäöüß";
		
		int[] buchstabenPositionen = new int[buchstaben.length()];
		int anzahl = 0;
		
		String dateiname = "langertext.txt";
		File datei = new File(dateiname);
		FileReader fr = new FileReader(datei);
		BufferedReader reader = new BufferedReader(fr);
		
		while (true) {
			String zeile = reader.readLine();
			if (zeile == null){
				break;
			} else {
				for (int i = 0; i < zeile.length(); i++){
					//KK: toLowerCase erzeugt in jedem Schleifendurchlauf einen neuen (identischen) String und sollte daher 
					//nur ein Mal pro Zeile (also vor der for-schleife) angewandt werden.
					char c = zeile.toLowerCase().charAt(i);
					int position = buchstaben.indexOf(c);
					if (position >= 0){
						buchstabenPositionen[position]++;
						anzahl++;
					}
				}
				
			}
		}
		
		reader.close();
		//KK: fr wurde nicht geschlossen. Aufgrund der Verkettung mit reader ist das ok (beide werden geschlossen wenn reader geschlossen wird).
		//Wegen der lesbarkeit sollte man es jedoch trotzdem noch einmal hinschreiben. Auch wissen nicht alle Leser von der close-Kette.
		
		
		System.out.println("Der Text enthält " + anzahl + " Zeichen.");
		System.out.println("");
		for (int j = 0; j < buchstaben.length(); j++){
			System.out.println("Buchstabe " + buchstaben.charAt(j) + " ist " + buchstabenPositionen[j] + " mal vorhanden.");
			
		}
		
		

	}

}
