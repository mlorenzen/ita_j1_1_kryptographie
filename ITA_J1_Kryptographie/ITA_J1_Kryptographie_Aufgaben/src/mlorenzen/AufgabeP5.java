package mlorenzen;

//KK: klasse gel�st
public class AufgabeP5 {
    public static void main (String args[]) {
        /* Der Test auf erreichen des Überlaufs erfolgt folgendermaßen:
         * Für jede Runde wird geprüft, ob das Ergebnis größer ist als 
         * der zulässige Maximalwert geteilt durch den Multiplikator der 
         * nächsten Runde. 
         * Falls das der Fall ist würde in der nächsten Runde ein Überlauf
         * stattfinden und die weitere Berechnung wird abgebrochen.
         */
    	
        int max = Integer.MAX_VALUE;
        int x = 1;
        int ergebnis = 1;
        
        while (true) {
            ergebnis = ergebnis * (x + 1);
            if (max / (x + 2) <= ergebnis) break; //KK: sehr sch�n!!
            x++;
        }
        System.out.println("Die größte darstellbare Zahl mit dem Typ int ist " + max);
        System.out.println("Die maximal moegliche Fakultaet mit dem Typ int in Java ist " + (int) (x+1) + "! mit dem Ergebnis " + ergebnis + "\n");
        
        //long
        long maxL = Long.MAX_VALUE;
        long xL = 1;
        long ergebnisL = 1;
        
        while (true) {
            ergebnisL = ergebnisL * (xL + 1);
            if (maxL / (xL + 2) <= ergebnisL) break; //KK: sehr sch�n!!
            xL++;
        }
        System.out.println("Die größte darstellbare Zahl mit dem Typ long ist " + maxL);
        System.out.println("Die maximal moegliche Fakultaet mit dem Typ long in Java ist " + (long) (xL + 1) + "! mit dem Ergebnis " + ergebnisL);
    }
}
