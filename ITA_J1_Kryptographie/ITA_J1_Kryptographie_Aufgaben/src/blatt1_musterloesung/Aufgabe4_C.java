package blatt1_musterloesung;

public class Aufgabe4_C
{

    public static void main(String[] args)
    {
        // 1. Variante
        System.out.println("Variante 1:");
        for(int x=1; x<71; x++)
        {
            if( Math.pow(191, x)%71 == 1.0 )
            {
                System.out.println("L�sung gefunden : " + x );
            }
        }
        
        // 2. Variante
        System.out.println("Variante 2:");
        for(int x=1; x<71; x++)
        {
            if( modexp(191,x,71) == 1)
                System.out.println("L�sung gefunden : " + x );
        }
    }
    
    /**
     * Benutzerdefinierte Methode zur Berechnung der modularen Exponentiation
     * 
     * Berechnet wird (basis^exp)%mod wobei bei der internen Berechnung bei
     * Zwischenergebnissen die mod-Operation angewendet wird, so dass die
     * Zahlen nicht "�berlaufen".
     * 
     * @param basis - Basis
     * @param exp   - Exponent   
     * @param mod   - Modul
     * @return Ergebnis
     */
    public static int modexp(int basis, int exp, int mod)
    {
        // Falls m�glich, wird Basis angepasst
        basis = basis%mod;
        
        // Ergebisvariable
        int res = basis;
        
        for( int n=2; n <= exp; n++ )
        {
            // Zwischenergebnisse werden immer durch modulo verkleinert
            // Dadurch bleiben die Zahlen "klein"
            res = Math.multiplyExact(res,basis)%mod;
        }
        
        return res;
    }
}
