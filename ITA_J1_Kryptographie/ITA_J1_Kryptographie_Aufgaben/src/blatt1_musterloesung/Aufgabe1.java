package blatt1_musterloesung;

public class Aufgabe1
{
    public static void main(String[] args)
    {
        // Buchstaben, die verschl�sselt werden.
        String buchstaben = "abcdefghijklmnopqrstuvwxyz����";
        
        // Zu verschl�sselndes Wort
        String originalString = "kryptographie";
        
        
        StringBuilder strBuilder = new StringBuilder();
        for( int i = 0; i < originalString.length(); i++)
        {
            char c = originalString.charAt(i);
            
            int index = buchstaben.indexOf(c);
            if( index >= 0 )
            {   
                // Buchstabe kommt in "abcdefghijklmnopqrstuvwxyz����" vor und
                // wird um drei Buchstaben nach rechts verschoben
                strBuilder.append( buchstaben.charAt( (index+3)%buchstaben.length() ) );
            }
            else
            {
                // Buchstabe kommt NICHT in "abcdefghijklmnopqrstuvwxyz����" vor und
                // wird ohne Modifikation �bernommen
                strBuilder.append( c );
            }
        }
        String chiffreString = strBuilder.toString();
        
        System.out.println("Original : " + originalString );
        System.out.println("Chiffre  : " + chiffreString );
    }
}
