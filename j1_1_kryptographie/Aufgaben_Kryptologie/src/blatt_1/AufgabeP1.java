package blatt_1;

public class AufgabeP1 {

	public static void main(String[] args) {
		
		String buchstaben = "abcdefghijklmnopqrstuvwxyzäöüß";
		String klartext = "kryptographie";
		StringBuilder chiffretext = new StringBuilder();
		
		for (int i = 0; i < klartext.length(); i++){
			
			char zeichen = klartext.charAt(i);
			int position = buchstaben.indexOf(zeichen);
			
			if (position >= 0){
				chiffretext.append(buchstaben.charAt((position + 3)%buchstaben.length()));
			}
			
		}
		
		String ergebnis = chiffretext.toString();
		System.out.println("Chiffre:  " + ergebnis);
		System.out.println("Klartext: " + klartext);

	}

}
