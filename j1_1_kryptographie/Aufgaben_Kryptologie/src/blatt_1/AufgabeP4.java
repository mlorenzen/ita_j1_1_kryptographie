package blatt_1;

public class AufgabeP4 {

	public static void main(String[] args) {
		//Sicher wäre ein Ablauf mit Schleife eleganter...
		
		// Teilaufgabe a)
		int r = 31;
		int a = 1234;
		int y = 1;
		boolean lösung = false;

		System.out.println("Teilaufgabe a)");

		for (int j = 1; j < r; j++){
		    if ((a * j)%r == y){
		        System.out.println("x = " + j + " ist eine Lösung.");
		        lösung = true;
		    }
		}
		if (!lösung) System.out.println("Es gibt keine Lösungen.");
		
		
		// Teilaufgabe b)
		r = 7;
		a = 11;
		y = 1;
		lösung = false;

		System.out.println("\nTeilaufgabe b)");

		for (int j = 1; j < r; j++){
		    if ((a^j)%r == y){
		        System.out.println("x = " + j + " ist eine Lösung.");
		        lösung = true;
		    }
		}
		if (!lösung) System.out.println("Es gibt keine Lösungen.");
		
		// Teilaufgabe c)
		r = 71;
		a = 191;
		y = 1;
		lösung = false;

		System.out.println("\nTeilaufgabe c)");

		for (int j = 1; j < r; j++){
		    if ((a^j)%r == y){
		        System.out.println("x = " + j + " ist eine Lösung.");
		        lösung = true;
		    }
		}
		if (!lösung) System.out.println("Es gibt keine Lösungen.");

	}

}
