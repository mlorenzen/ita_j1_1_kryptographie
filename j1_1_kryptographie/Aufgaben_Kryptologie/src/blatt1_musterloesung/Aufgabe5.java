package blatt1_musterloesung;

public class Aufgabe5
{
  // Hier wird auf eine neue Methode zur�ck gegriffen, die mit dem JDK 8 eingef�hrt wurde
  // Math.multiplyExact wirft eine Exception, wenn es zu einem �berlauf kommt.
  // -> Die Methode wurde in der Vorlesung nicht besprochen!!!!
  //
  // Alternativ m�sste man bei der Multiplikation pr�fen, ob man in den negativen Bereich
  // l�uft.
  public static void main(String[] args)
  {
    System.out.println("Max int  : " + Integer.MAX_VALUE);
    System.out.println("Max long : " + Long.MAX_VALUE);

    // Test int-Variable
    try
    {
      int ifaku = 1;
      for (int i = 2; i < 100; i++)
      {
        ifaku = Math.multiplyExact(ifaku, i);
        System.out.println(i + "! = " + ifaku);
      }
    } catch (ArithmeticException exce)
    {
      System.out.println("Ein �berlauf ist aufgetreten");
    }

    // Test long-Variable
    try
    {
      long lfaku = 1;
      for (long l = 2; l < 100; l++)
      {
        lfaku = Math.multiplyExact(lfaku, l);
        System.out.println(l + "! = " + lfaku);
      }
    } catch (ArithmeticException exce)
    {
      System.out.println("Ein �berlauf ist aufgetreten");
    }
  }

}
