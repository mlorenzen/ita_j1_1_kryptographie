package blatt1_musterloesung;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Aufgabe2_B
{

    public static void main(String[] args) throws Exception
    {
        // Buchstaben, die gez�hlt werden
        String buchstaben = "abcdefghijklmnopqrstuvwxyz����";
        
        //Array, in dem die H�ufigkeiten gez�hlt werden
        int[] count = new int[ buchstaben.length() ];
        
        // Eingabedatei �ffnen
        String datei = "DerRasendeRudi.txt";
        File file = new File(datei);
        FileReader fr = new FileReader(file);
        BufferedReader reader = new BufferedReader( fr );
        
        // Allgemeine Z�hlvariable f�r die Buchstaben
        int buchstabenCount = 0;
        
        String line = null;
        while((line = reader.readLine()) != null) 
        {
           // Konvertiere alle Buchstaben in Kleinbuchstaben 
           String lowerLine = line.toLowerCase();
           for(int i=0; i < lowerLine.length(); i++ )
           {
               char c = lowerLine.charAt(i);
               int pos = buchstaben.indexOf(c);
               // Buchstabe gefunden
               if( pos >= 0 )
               {
                   count[pos]++;
                   buchstabenCount++;
               }
           }
        }
        
        // Ressourcen schlie�en
        reader.close();
        fr.close();

        // Ausgabe der absoluten und relativen H�ufigkeiten
        System.out.println("Buchstabenh�ufigkeit in der Datei " + datei );
        System.out.println("Anzahl der Buchstaben: " + buchstabenCount );
        for(int i=0; i< buchstaben.length(); i++)
        {
            System.out.println( buchstaben.charAt(i) + " : " + count[i] + " (" + String.format( "%.2f", (100.0*count[i])/buchstabenCount) + "%)");
        }
    }

}
