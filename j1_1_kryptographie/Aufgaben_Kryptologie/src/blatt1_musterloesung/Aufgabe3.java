package blatt1_musterloesung;

public class Aufgabe3
{
    public static void main(String[] args)
    {
        System.out.println("12 mod 7 = " + 12%7 );
        System.out.println("-12 mod 7 = " + (-12%7) );
        
        // Liefert falsches Ergebnis!!!
        System.err.println("FALSCH:  7^32 mod 5 = " + Math.pow(7,32)%5 );

        // Korrektes Ergebnis
        System.out.println("7^32 mod 5 = " + Math.pow(2,32)%5 );
        System.out.println("7^32 mod 5 = " + modexp(7,32,5) );
    }

    
    /**
     * Benutzerdefinierte Methode zur Berechnung der modularen Exponentiation
     * 
     * Berechnet wird (basis^exp)%mod wobei bei der internen Berechnung bei
     * Zwischenergebnissen die mod-Operation angewendet wird, so dass die
     * Zahlen nicht "�berlaufen".
     * 
     * @param basis - Basis
     * @param exp   - Exponent   
     * @param mod   - Modul
     * @return Ergebnis
     */
    public static int modexp(int basis, int exp, int mod)
    {
        // Falls m�glich, wird Basis angepasst
        basis = basis%mod;
        
        // Ergebisvariable
        int res = basis;
        
        for( int n=2; n <= exp; n++ )
        {
            // Zwischenergebnisse werden immer durch modulo verkleinert
            // Dadurch bleiben die Zahlen "klein"
            res = Math.multiplyExact(res,basis)%mod;
        }
        
        return res;
    }
}
