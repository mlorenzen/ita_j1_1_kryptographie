package blatt1_musterloesung;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Aufgabe2_A
{

    public static void main(String[] args) throws Exception
    {
        // Eingabedatei �ffnen
        String datei = "DerRasendeRudi.txt";
        File file = new File(datei);
        FileReader fr = new FileReader(file);
        BufferedReader reader = new BufferedReader( fr );
        
        // Z�hlvariable f�r die Buchstaben
        int buchstabenCount = 0;
        
        String line = null;
        while((line = reader.readLine()) != null) 
        {
           String lowerLine = line.toLowerCase();
           for(int i=0; i < lowerLine.length(); i++ )
           {
               if( Character.isLetter( lowerLine.charAt(i)))
               {
                   buchstabenCount++;
               }
           }
        }
        
        // Ressourcen schlie�en
        reader.close();
        fr.close();

        System.out.println("Die Datei " + datei + " enth�lt " + buchstabenCount + " Buchstaben");
    }

}
