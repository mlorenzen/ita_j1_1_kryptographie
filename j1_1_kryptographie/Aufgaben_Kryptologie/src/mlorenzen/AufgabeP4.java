package mlorenzen;

//KK: Sie hatten eine Variable namens "loesung".
//Quellcode sollte keine Umlaute oder Sonderzeichen enthalten (Ausnahme in Strings).
//Aufgrund von verschiedenen Zeichenkodierungen, kommt es dadurch immer wieder zu Problemen.
//Die Klasse lies sich bei mir nicht ausf�hren ohne dass ich den Namen ge�ndert habe.
public class AufgabeP4 {

	public static void main(String[] args) {
		//Sicher wäre ein Ablauf mit Schleife eleganter... 
		//KK: Sie haben es doch sehr elegant mit Schleifen gel�st? =) 
		
		// Teilaufgabe a)
		int r = 31;
		int a = 1234;
		int y = 1;
		boolean loesung = false;

		System.out.println("Teilaufgabe a)");

		//KK: sehr sch�n
		for (int j = 1; j < r; j++){
		    if ((a * j)%r == y){
		        System.out.println("x = " + j + " ist eine Loesung.");
		        loesung = true;
		    }
		}
		if (!loesung) System.out.println("Es gibt keine Loesungen.");
		
		
		// Teilaufgabe b)
		r = 7;
		a = 11;
		y = 1;
		loesung = false;

		System.out.println("\nTeilaufgabe b)");
		//KK: w�re korrekt, wenn statt a^j -> Math.pow(a,j) genutzt wurde. ^ ist nicht der Potenzoperator
		for (int j = 1; j < r; j++){
		    if ((a^j)%r == y){
		        System.out.println("x = " + j + " ist eine Loesung.");
		        loesung = true;
		    }
		}
		if (!loesung) System.out.println("Es gibt keine Loesungen.");
		
		// Teilaufgabe c)
		r = 71;
		a = 191;
		y = 1;
		loesung = false;

		System.out.println("\nTeilaufgabe c)");
		//KK: gleiches Problem wie bei b). Allerdings noch mehr:
		//KK: hier kommt wieder die Problematik des �berlaufs ins Spiel.
		//wie bei Aufgabe 3. Das Ergebnis der Potenzrechnung ist zu gro�
		//f�r den int-Raum. Daher hier wieder "safe" modulo rechnen indem man nach jeder Teilmultiplikation 
		//mit Modulo die Zahl klein h�lt. Siehe Musterl�sung
		for (int j = 1; j < r; j++){
		    if ((a^j)%r == y){
		        System.out.println("x = " + j + " ist eine Loesung.");
		        loesung = true;
		    }
		}
		if (!loesung) System.out.println("Es gibt keine Loesungen.");

	}

}
