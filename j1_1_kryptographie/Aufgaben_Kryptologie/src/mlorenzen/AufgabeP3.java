package mlorenzen;

public class AufgabeP3 {

	public static void main(String[] args) {
		
		//KK: korrekt
		int a1 = 12%7;
		
		//KK: korrekt, -5 ist genau so richtig wie 2 (handschriftlich), da sich beide Zahlen in der gleichen Restklasse von 7 befinden. 
		//Welche der beiden letztendlich errechnet wird, ist Programmiersprachen-Abh�ngig
		int a2 = -12%7;
		
		//KK;
		//Um mit Potenzen zu rechnen muss man entweder manuell a*a (und das x mal) rechnen.
		//Dazu eignet sich dann entsprechend eine Schleife gut, anstatt es h�ndisch 32x hinzuschreiben ;)
		//Oder man nutzt (�blicherweise) Math.pow(basis, exponent).
		//Allerdings n�tzt hier Math.pow auch wenig.
		//Begr�ndung:
		//Die Datentypen int bzw. double k�nnen die 7^32 nicht darstellen, da die Zahl zu gro� ist.
		//MAX_VALUE von int: (2^31)-1  <  7^32
		//MAX_VALUE von double: (2^63)-1 < 7^32
		//Die 7^32 �berschreitet somit die Grenze des Datentyps und l�uft in den Negativen Bereich �ber, was ein falsches Ergebnis liefert.
		//Eine L�sung ist es, einen gro�en Datentypen wie BigInteger zu nutzen, welche die Zahl darstellen kann.
		//Alternativ zu BigInteger kann nach jeder multiplikation a*a der 32 multiplikationen ein mal modulo anwenden um die Zahl klein zu halten.
		//Siehe Musterl�sung.
		int a3 = (7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7*7)%5;
		
		System.out.println("Ergebnisse des Java-modulo-Befehls:");
		System.out.println("12 mod 7 = " + a1);
		System.out.println("-12 mod 7 = " + a2);
		System.out.println("7E32 mod 5 = " + a3);
		

	}

}
