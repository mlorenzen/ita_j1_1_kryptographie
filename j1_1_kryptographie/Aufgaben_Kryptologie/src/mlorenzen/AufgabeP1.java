package mlorenzen;

//KK: gut =)
public class AufgabeP1 {

	//KK: Etwas stimmt mit Ihrer Zeichenkodierung nicht. Bei mir k�nnen die von ihnen geschriebenen Umlaute nicht dargestellt werden.
	//IdR ist die Standardkodierung Cp1252 bei Eclipse eingestellt. Stellen Sie sicher, dass das bei Ihnen der Fall ist.
	//siehe: http://stackoverflow.com/questions/9180981/how-to-support-utf-8-encoding-in-eclipse (statt utf-8 -> Cp1252)
	public static void main(String[] args) {
		
		String buchstaben = "abcdefghijklmnopqrstuvwxyzäöüß";
		String klartext = "kryptographie";
		StringBuilder chiffretext = new StringBuilder();
		
		//KK: bei einer Eingabe durch den Benutzer m�sste man die Eingabe noch auf Kleinbuchstaben setzen (string.toLowerCase())
		
		for (int i = 0; i < klartext.length(); i++){
			
			char zeichen = klartext.charAt(i);
			int position = buchstaben.indexOf(zeichen);
			
			if (position >= 0){
				chiffretext.append(buchstaben.charAt((position + 3)%buchstaben.length()));
			}
			//KK: hier k�nnte man die nicht enthaltenen Buchstaben noch appenden, 
			//da sie sonst vollkommen verloren gehen (z.B. Leerzeichen). 
			//Dadurch kommt beim Entschl�sseln evtl. unlesbarer Text raus.
			
		}
		
		String ergebnis = chiffretext.toString();
		System.out.println("Chiffre:  " + ergebnis);
		System.out.println("Klartext: " + klartext);

	}

}
