package blatt2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class AufgabeP1 {

	public static void main(String[] args) throws Exception {
		/* Programm zur Verschlüsselung einer Textdatei mit der 
		 * Vigenere-Verschlüsselung.
		 */

		String buchstaben = "abcdefghijklmnopqrstuvwxyzäöüß";
		String schluessel = "ganzgeheim";
		String klartext;
		String chiffre;
		
		// Datei öffnen
		String dateiName = "langertext.txt";
		File textDatei = new File(dateiName);
		FileReader fr = new FileReader(textDatei);
		BufferedReader reader = new BufferedReader(fr);
		
		// Datei einlesen 
		String zeile = null;
		StringBuilder kleinerText = new StringBuilder();
		while (true){
			zeile = reader.readLine();
			if (zeile == null) break;
			kleinerText.append(zeile.toLowerCase());
			kleinerText.append(System.lineSeparator());
		}
		
		// Ressourcen schließen
		reader.close();
		fr.close();
		
		klartext = kleinerText.toString();
		StringBuilder chiffreBuilder = new StringBuilder();

		//Verschlüsselung
		for (int i = 0; i < klartext.length(); i++){
			char c = klartext.charAt(i);
			if (buchstaben.indexOf(c) >= 0){
				chiffreBuilder.append(zeichenVerschluesseln(c, schluessel, buchstaben, (i % schluessel.length())));
			} else {
				chiffreBuilder.append(c);
			}
		}	
		chiffre = chiffreBuilder.toString();
		System.out.println(chiffre);
		

	}

	private static char zeichenVerschluesseln(char z, String schluessel, String alphabet, int position){
		int positionInAlphabet = alphabet.indexOf(z);
		int verschiebung = alphabet.indexOf(schluessel.charAt(position));
		int neuePosition = (positionInAlphabet + verschiebung) % alphabet.length();
		return alphabet.charAt(neuePosition);
	}
}
