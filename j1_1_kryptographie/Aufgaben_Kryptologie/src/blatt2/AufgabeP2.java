package blatt2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class AufgabeP2 {

	public static void main(String[] args) throws Exception {
		String fileContent = getFileContentLc("geheimC-UTF8.txt");
		String alphabet = "abcdefghijklmnopqrstuvwxyzäöüß";
		
		int[] nI = new int[alphabet.length()];
		int n = 0;
		
		/// Anzahl der Buchstaben ermitteln
		for (int i = 0; i < fileContent.length(); i++){
			char c = fileContent.charAt(i);
			if (alphabet.indexOf(c) >= 0) {
				n++;
				int position = alphabet.indexOf(c);
				nI[position]++;
			}
		}
		/* Debug-Ausgaben
		System.out.println("Der Text hat " + n + " Zeichen");
		
		for (int i = 0; i < alphabet.length(); i++){
			System.out.println(alphabet.charAt(i) + " - " + nI[i]);
		}
		*/
		/// Koinzidenzindex berechnen
		double I; 
		int zaehler = 0;
		for (int i = 1; i <= alphabet.length(); i++){
			zaehler = zaehler + nI[i-1] * ((nI[i-1] - 1));
		}
		I = (double) zaehler / ((double) n * (n - 1));
		System.out.println("Der Koinzidenzindex für den Text ist " + I);
		
		///Schlüssellänge berechenen
		double h = (0.0377 * n) /(I * (double) (n - 1) - 0.0385 * (double) n + 0.0762);
		System.out.println("Die berechnete Schlüsselänge ist " + h);
		
	}
	
	private static String getFileContentLc(String dateiName) throws Exception{
		/// liest eine Textdatei und gibt den Inhalt in kleinschreibung zurück
		File textDatei = new File(dateiName);
		FileReader fr = new FileReader(textDatei);
		BufferedReader reader = new BufferedReader(fr);
		String line = null;
		StringBuilder textCollector = new StringBuilder();
		while (true){
			line = reader.readLine();
			if (line == null) break;
			textCollector.append(line.toLowerCase());
			textCollector.append(System.lineSeparator());
		}
		reader.close();
		fr.close();
		return textCollector.toString();
	}

}
